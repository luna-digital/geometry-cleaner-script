# Geometry Cleaner Script for Blender #

This script does a basic pass over the Blender scene and cleans up object data. It's ideal for optimizing geometry imported as FBX, STL, or other intermediate formats before using in Blender.

### What exactly does it do? ###

* Removes custom normals data
* Clears any parenting and applies transforms
* Sets autosmooth
* Recalculates normals
* Converts tris to quads
* Removes doubles

### How to use it ###

* Open the geometry_cleaner.py script in Blender's script window.
* Import your FBX, STL, or other file into Blender.
* Click the "Run Script" button.

**Note:** If you have a lot of objects in your scene, this might take a while. Blender's UI will lock while the script is running. Be sure to check the system console for status updates, including a percentage counter, estimated time remaining, and the current active object being cleaned.

### Get in touch ###
For questions about this script, or how to contribute, email us at hello@lunadigital.tv.