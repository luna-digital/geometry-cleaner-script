'''
    Geometry Cleaner Script
    
    Does a basic pass over the Blender scene and cleans up object data. Ideal for optimizing geometry
    imported as FBX, STL, or other intermediate formats before using in Blender.
    
    What does it do?
        - Removes custom normals data
        - Clears any parenting and applies transforms
        - Sets autosmooth
        - Recalculates normals
        - Converts tris to quads
        - Removes doubles
        
    How to use it:
        1. Open the geometry_cleaner.py script in Blender's script window.
        2. Import your FBX, STL, or other file into Blender.
        3. Click the "Run Script" button.
    
    Note:
        If you have a lot of objects in your scene, this might take a while. Blender's UI will lock while the script
        is running. Be sure to check the system console for status updates, including a percentage counter, estimated
        time remaining, and the current active object being cleaned.
    
    Author: Aaron Powell (aaron@lunadigital.tv)
    Version: 0.1.0
    License: GPL v3
'''

import bpy
import math
import datetime

'''
Get percentage of files processed

Parameters: 'params' as dictionary
Returns: float
'''
def get_percentage(params):
    return math.floor(
        (params['processed'] / params['total']) * 100
    )

'''
Get estimated time remaining in minutes

Parameters: 'params' as dictionary
Returns: float
'''
def get_time_remaining(params):
    delta = datetime.datetime.today() - params['start_time']
    
    if (params['processed'] == 0):
        return -1
    else:
        time_per_object = delta.total_seconds() / params['processed'] / 60
        return time_per_object * (params['total'] - params['processed'])

'''
Print statistics for object

Parameters: 'params' as dictionary, 'ob' as Blender object
Returns: nothing
'''
def print_statistics(params, ob):
    percentage = get_percentage(params)
    
    time_remaining = get_time_remaining(params)
    if (time_remaining == -1):
        time_remaining = "---"
    else:
        time_remaining = round(time_remaining, 2)
        
    print(
        "({0}% / {1} min remaining) Processing '{2}'...".format(
            str(percentage),
            str(time_remaining),
            ob.name
        )
    )

'''
Cleans object-specific data

Parameters: 'ob' as Blender object
Returns: nothing
'''
def clean_object_data(ob):
    # Ensure object mode
    if ob.mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT')
    
    # Set object as active
    bpy.context.view_layer.objects.active = ob
    
    # Remove custom normals
    bpy.ops.mesh.customdata_custom_splitnormals_clear()
    
    # Remove parenting and normalize transforms
    bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
    bpy.ops.object.transform_apply(location=False, rotation=True, scale=True)
    
    # Set autosmooth
    bpy.context.object.data.use_auto_smooth = True

'''
Cleans geometry-specific data

Parameters: 'ob' as Blender object
Returns: nothing
'''
def clean_geometry_data(ob):
    # Ensure edit mode
    if ob.mode != 'EDIT':
        bpy.ops.object.mode_set(mode='EDIT')
        
    bpy.ops.mesh.select_all(action='SELECT')
                
    # Recalculate normals
    bpy.ops.mesh.normals_make_consistent(inside=False)
                
    # Convert tris to quads
    bpy.ops.mesh.tris_convert_to_quads()
            
    # Remove doubles
    bpy.ops.mesh.remove_doubles()
    
    # Return to object mode before exiting
    bpy.ops.object.mode_set(mode='OBJECT')
    
'''
Clean scene meshes
Returns: nothing
'''
def clean(selected_only=False):
    view_layer = bpy.context.view_layer
    
    params = {
        "processed": 0,
        "total": len(view_layer.objects),
        "start_time": datetime.datetime.today()
    }
    
    for ob in view_layer.objects:
        if ob.type == 'MESH':
            print_statistics(params, ob)
            clean_object_data(ob)
            clean_geometry_data(ob)
            
            params['processed'] += 1
            
    bpy.ops.object.select_by_type(type='EMPTY')
    bpy.ops.object.delete(use_global=False)

    print("\nDone!\n")

if __name__ == '__main__':
    clean()